﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReplaceInFiles
{
    public partial class Form1 : Form
    {
        private List<string> files = new List<string>();
        public Form1(string fileName)
        {
            InitializeComponent();
            AddFile(fileName);
        }
        public Form1(IEnumerable<string> fileNames)
        {
            InitializeComponent();
            AddFiles(fileNames);
            
        }

        private void AddFiles(IEnumerable<string> fileNames)
        {
            foreach (var fileName in fileNames)
            {
                AddFile(fileName);
            }
        }

        private OpenFileDialog ofd;
        public void AttachOpenFileDialog(OpenFileDialog openFileDialog)
        {
            ofd = openFileDialog;
        }
        private void AddFile(string fileName)
        {
            if (files.Contains(fileName)) return;
            files.Add(fileName);
            var fi = new System.IO.FileInfo(fileName);
            var lvi = lvwFiles.Items.Add(files.Count.ToString());
            var details = new string[lvi.ListView.Columns.Count+1];
            details[colFileName.Index-1] = fi.Name;
            details[colFileLength.Index-1] = fi.Length.ToString();
            details[colFileNewLength.Index - 1] = "";
            lvi.SubItems.AddRange(details);
            UpdateNewLength(lvi.Index);
        }
        private void UpdateNewLength(int index)
        {
            var fileName = files[index];
            var fi = new System.IO.FileInfo(fileName);
            lvwFiles.Items[index].SubItems[colFileNewLength.Index].Text = fi.Length.ToString();
        }
        struct IResult
        {
            public bool success { get; set; }
            public string message { get; set; }
            public string fileName { get; set; }
            public long replacements { get; set; }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (lvwFiles.Items.Count == 0)
            {
                MessageBox.Show("Please select a file File!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            var failures = new List<IResult>();
            var eachLineReplacement = chkEachLineReplacement.Checked;
            string search = txtSearch.Text;
            string replace = txtReplace.Text;
            if (eachLineReplacement)
            {
                string[] searchLines = Split(txtSearch.Text);
                string[] replaceLines = Split(txtReplace.Text);
                if (searchLines.Length != replaceLines.Length)
                {
                    MessageBox.Show("'Search' and 'Replace with' must have same amount of lines!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            long totalReplacements = 0;
            int fileCount = 0;
            foreach (ListViewItem lvi in lvwFiles.Items)
            {
                var fileName = files[lvi.Index];
                var result = processFile(fileName, eachLineReplacement, search, replace);
                UpdateNewLength(lvi.Index);
                totalReplacements += result.replacements;
                if (result.success) fileCount += 1;
            }

            MessageBox.Show($"{totalReplacements} replacements applied in {fileCount} file(s)!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private IResult processFile(string fileName, bool eachLineReplacement, string inputSearch, string inputReplace)
        {
            var sr = new System.IO.StreamReader(fileName, System.Text.Encoding.Default, true);
            var encoding = sr.CurrentEncoding;
            string fileContent = sr.ReadToEnd();
            sr.Close();
            string newFileContent = "";
            if (eachLineReplacement)
            {
                string[] searchLines = Split(txtSearch.Text);
                string[] replaceLines = Split(txtReplace.Text);
                int replacements = 0;
                for (int i = 0; i < searchLines.Length; i++)
                {
                    string search = searchLines[i];
                    string replacement = replaceLines[i];
                    if (string.IsNullOrEmpty(search)) continue;
                    newFileContent = fileContent.Replace(search, replacement);
                    if (string.Compare(newFileContent, fileContent, false) != 0)
                    {
                        replacements += 1;
                        fileContent = newFileContent;
                    }
                }
                System.IO.File.WriteAllText(fileName, fileContent, encoding);
                return new IResult { success = true, fileName = fileName, message = "", replacements = replacements };
            }
            newFileContent = fileContent.Replace(inputSearch, inputReplace);
            System.IO.File.WriteAllText(fileName, newFileContent, encoding);
            return new IResult { success = true, fileName = fileName, message = "", replacements = 1 };
        }

        private string[] Split(string arg)
        {
            return new System.Text.RegularExpressions.Regex("\r?\n").Split(arg);
        }

        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            if(ofd.ShowDialog() == DialogResult.OK)
            {
                AddFiles(ofd.FileNames);
            }
        }

        private void lvwFiles_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnRemoveFile.Enabled = lvwFiles.SelectedItems.Count > 0;
        }

        private void btnRemoveFile_Click(object sender, EventArgs e)
        {
            var index = lvwFiles.SelectedItems[0].Index;
            while (lvwFiles.SelectedItems.Count > 0)
            {
                lvwFiles.SelectedItems[0].Remove();
                files.RemoveAt(index);
            }
            for (int i = index; i < lvwFiles.Items.Count; i++)
            {
                lvwFiles.Items[i].SubItems[colIndex.Index].Text = index.ToString();
            }
        }
    }
}
