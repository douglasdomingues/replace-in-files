﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ReplaceInFiles
{
    public partial class mdiMain : Form
    {
        int count = 0;
        public mdiMain()
        {
            InitializeComponent();
        }

        private void OpenFile(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = GetOFDLastDir();
            openFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
            openFileDialog.Multiselect = true;
            openFileDialog.FileOk += OpenFileDialog_FileOk;
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                Form1 fm = new Form1(openFileDialog.FileNames);
                fm.Text = $"Replacer #{++count}";
                fm.MdiParent = this;
                fm.AttachOpenFileDialog(openFileDialog);
                fm.Show();
                                
            }
        }

        private void OpenFileDialog_FileOk(object sender, CancelEventArgs e)
        {
            var ofd = sender as OpenFileDialog;
            var fi = new System.IO.FileInfo(ofd.FileName);
            Properties.Settings.Default.OFDLastDir = fi.DirectoryName;
            Properties.Settings.Default.Save();
        }

        private string GetOFDLastDir()
        {
            var directory = Properties.Settings.Default.OFDLastDir;
            if(string.IsNullOrEmpty(directory)) return Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            return directory;
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CutToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(ActiveForm.ActiveControl is TextBox && Clipboard.ContainsText())
            {
                TextBox tb = ActiveForm.ActiveControl as TextBox;
                
                tb.Text = Clipboard.GetText();
            }
        }

        private void ToolBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStrip.Visible = toolBarToolStripMenuItem.Checked;
        }

        private void StatusBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            statusStrip.Visible = statusBarToolStripMenuItem.Checked;
        }

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

        private void toolStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
    }
}
